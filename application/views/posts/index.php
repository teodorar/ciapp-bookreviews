<br><h2><?= $title ?></h2><br>
<?php foreach($posts as $post) : ?>

<div class="jumbotron">	
	<h3 class="display-5"><?php echo $post['title']; ?></h3>
	<small><span class="badge badge-info" class="post-date">Posted on: <?php echo $post['created_at']; ?></span></small><br>
	<hr class ="my-4">
	<p>  <?php echo $post['body']; ?> </p>
	<br><br>
	<p><a class="btn btn-primary btn-lg"href="<?php echo site_url('/posts/'.$post['slug']); ?>">Read More</a></p>
<?php endforeach; ?>

<?php echo $this->pagination->create_links(); ?>


