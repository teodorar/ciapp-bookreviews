<html>
	<head>
	<title>Book Reviews</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
	</head>
	<body>
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="<?php echo base_url(); ?>home">Book Reviews</a>
		</div>
		<div class="collapse navbar-collapse" id="navbarColor1">
			<ul class="nav navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"href="<?php echo base_url(); ?>home">Home<span class="sr-only">(current)</span></a></li>
				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>about">About</a></li>
				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>posts">Reviews</a></li>
				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>categories">Search by Category</a></li>
				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>ratings">Search by Rating</a></li>
			</ul>
			<ul class="nav navbar-nav">
				<?php if(!$this->session->userdata('logged_in')) : ?>
					<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>users/login">Login</a></li>
					<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>users/register">Become a Reviewer</a></li>
				<?php endif; ?>
				<?php if($this->session->userdata('logged_in')) : ?>
				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>posts/create">Post Review</a></li>
				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>categories/create">Create Category</a></li>
				<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>users/logout">Log out</a></li>
				<?php endif; ?>
				<hr class ="my-4">
		</div>
	</div>
	</nav> 

	<div class="container">
		<!--Flash message-->
		<?php if($this->session->flashdata('user_registered')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_registered').'</p>'; ?>
		<?php endif; ?>

		<?php if($this->session->flashdata('post_created')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_created').'</p>'; ?>
		<?php endif; ?>

		<?php if($this->session->flashdata('post_updated')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_updated').'</p>'; ?>
		<?php endif; ?>

		<?php if($this->session->flashdata('category_created')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('category_created').'</p>'; ?>
		<?php endif; ?>

		<?php if($this->session->flashdata('post_deleted')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_deleted').'</p>'; ?>
		<?php endif; ?>

		<?php if($this->session->flashdata('login_failed')): ?>
			<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
		<?php endif; ?>
		<?php if($this->session->flashdata('user_loggedin')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>'; ?>
		<?php endif; ?>
		<?php if($this->session->flashdata('user_loggedout')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedout').'</p>'; ?>
		<?php endif; ?>
		


	