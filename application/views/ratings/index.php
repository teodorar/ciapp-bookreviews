<br><h2><?= $title; ?></h2><br>

<ul class="list-group">
<?php foreach($ratings as $rating) : ?>
	<li class="list-group-item"><a href="<?php echo site_url('/ratings/posts/'.$rating['id']); ?>"><?php echo $rating['nameR']; ?></a></li>
<?php endforeach; ?>
</ul>