<br>
<h2>Welcome back</h2><br>
<?php echo form_open('users/login'); ?>
	<div class="form-group">
		<input type="text" name="username" class="form-control" placeholder="Enter Username" required autofocus>
	</div>
	<div class="form-group">
		<input type="password" name="password" class="form-control" placeholder="Enter Password" required autofocus>
	</div>
	<button type="submit" class="btn btn-primary">Login</button>

<?php echo form_close(); ?>