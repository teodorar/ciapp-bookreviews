<?php
	class Ratings extends CI_Controller{
		public function index(){
			$data['title'] = 'Ratings';
			$data['ratings'] = $this->rating_model->get_ratings();

			$this->load->view('templates/header');
			$this->load->view('ratings/index', $data);
			$this->load->view('templates/footer');
		}

		public function posts($id){
			$data['title'] = $this->rating_model->get_rating($id)->nameR;

			$data['posts'] = $this->post_model->get_posts_by_rating($id);
			$this->load->view('templates/header');
			$this->load->view('posts/index', $data);
			$this->load->view('templates/footer');
 		}
	}