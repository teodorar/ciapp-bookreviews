<?php

//class with main controller that all of controllers have to extend
	class Pages extends CI_Controller{
		public function view($page = 'home'){
			//apppath- codeigniter constant that gives path to our app folder
			if(!file_exists(APPPATH.'views/pages/'.$page. '.php')){
				//codeigniter function to go to 404 error
				show_404();
			}
			//variable inside a data array; variables that we want to pass into view
			//ucfirst - function for capital letter
			$data['title'] = ucfirst($page);

			$this->load->view('templates/header');
			$this->load->view('pages/'.$page, $data);
			$this->load->view('templates/footer');

		}

	}